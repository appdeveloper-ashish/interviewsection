Android Interview Questions

**Q.1. What is the lifecycle of Activity?**
**Ans.:**

`onCreate()`
`onStart()`
`onResume()`

// Activity is visible now.
// When user press back, or activity goes in background by any reason

`onPause()`
`onStop()`
`onDestroy()`


*If Activity has been stopped by calling `onStop()` and `onDestroy()` has not been called then `onRestart()` will be executed followed by `onStart(), onResume()`."*

**Q.2. What is the super class of Activity?**
**Ans.:** `Object` -> `Context` -> `ContextWrapper` -> `ContextThemeWrapper` -> `Activity`

**Q.3. What is super class of AppCompatActivity?**
**Ans.:** Object -> Context -> ContextWrapper -> ContextThemeWrapper -> Activity - FragmentActivity (v4) -> AppCompatActivity (v7)

**Q.4. What is Intent?**
**Ans.:** `Object -> Intent`

Intent is an abstract description of an operation to be performed. Its most significant use to start components like Activity, Service etc.

**Q.5. What are the Implicit Intent and Explicit Intent?**
**Ans.:** **Implicit Intents** do not directly specify the component name that need to be called, it only specifies the action to be performed.
**Explicit intent** specifies the qualified name of the component we need to start.

**Q.6. What are the launch modes of an activity?**
**Ans.:** Launch modes are the instruction for Android OS which specifies how the activity should be launched. 
It instructs how a new activity should be associated with current task.
There are four types of launch modes of an activity in Android. 

**TASK AFFINITY** *Task Affinity defines which task an activity belongs to. By default an activity has same task affinity as it's root activity.

**1) standard:** Allows multiple instance of an activity in same and another task. it is default actvity launcher mode.

**2) singleTop:** Using this launch mode you can create multiple instance of the same activity in the same task or in different tasks only if 
the same instance does not already exist at the top of stack. 

**3) singleTask:** 
WITH TASK AFFINITY:

- Single task activity will always be at root of task
- There can only be one single task activity across task.
- If an instance of activity exists on the separate task, a new instance will not be created and Android system routes the intent information through onNewIntent() method. 
- At a time only one instance of activity will exist.

WITHOUT TASK AFFINITY:
- Single task activity need not be at root of the task.
- Single task activity will not launched in new task.
- There can only be one single task activity across task.


**4) singleInstance:** Single instance activity will be launched in separate task, and it will be only single activity in that task.

If task affinity is being used with single instance then, user can switch between tasks in mobile, can not switch otherwise.

**Q.7. What is doze mode?**
**Ans.:** Doze reduces battery consumption by deferring background, CPU and network activity for apps when the device is unused for long periods of time. App Standby defers background network activity for apps with which the user has not recently interacted.

**Q.8. Can we use Fragment without UI? If yes, how?**
**Ans.:** Headless Fragments, have one really useful feature - they can be retained by the FragmentManager across configuration changes. Since they do not have any UI related to them, they do not have to be detroyed and rebuilt again when the user rotates the device for example. In order to activate this behaviour, one just has to set the retained flag of the Fragment when it is initialized. This can be done in the onCreate method of the Fragment.

Below is example:

```java
@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);
}
```



**Q.9. What is Handler?**
**Ans.:**

**Q.10. What is message queue in android?**
**Ans.:**

**Q.11. What is Intent Service?**
**Ans.:** 

**Q.10. What is message queue in android?**
**Ans.:**

**Q.11. What is message queue in android?**
**Ans.:**

