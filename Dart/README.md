# DART INTERVIEW SECTION #

Below are the important questions of the Dart.


**Q.1: What is the features of the Dart language?**

**Ans.:** 
**1- Open Source**
**2- Platform Independent**
**3- Object-Oriented**
**4- Concurrency:** Dart is an asynchronous programming language, which means it supports multithreading using Isolates. The isolates are the independent entities that are related to threads but don't share memory and establish the communication between the processes by the message passing.
**5- Extensive Libraries**
**6- Flexible Compilation**
**7- Type Safe:** The Dart is the type safe language, which means it uses both static type checking and runtime checks to confirm that a variable's value always matches the variable's static type, sometimes it known as the sound typing.
**8- Browser Support:** The Dart supports all modern web-browser. It comes with the dart2js compiler that converts the Dart code into optimized JavaScript code that is suitable for all type of web-browser.

**Q.2: What are the data types supported by the Dart?**

**Ans.:** 
1. Number: int, double, num.
2. String
3. Boolean: bool.
4. Lists: List
5. Maps: Map    

**Q.3: What is the mixin?**

**Ans.:** In object-oriented programming languages, a Mixin is a class that contains methods for use by other classes without having to be the parent class of those other classes.
In other words mixins are normal classes from which we can borrow methods(or variables) without extending the class. In dart we can do this by using the keyword with.

Let's understand this with help of the example of different people with different occupations: artist, engineer, doctor, and sportsman. Let's think in terms of OOP manner, representing people in their classes.

- `Person` class: The most common properties could be abstracted in class Person that other classes would extend from. Common properties (like age , name etc) and common behaviors (like eat and sleep) can go in here.
```dart
abstract class Person {
  int age;
  int name;

  eat() {}
  sleep() {}
}
```
- `Artist` class: Our artist is a Person. Makes landscapes sketches.
- `Engineer` class: The engineer is a Person. Makes buildings sketches and reads research papers on construction.
- `Doctor` class: The doctor is a Person. Reads daily on latest health news and likes to do workout.
- `Boxer` class: The boxer is a type of Athlete. Athlete is a Person. Boxer does routine exercises as well practices punches.

As we see from above that artist does sketching. Engineer does sketching as well as reading. Doctor does reading and exercise. The athlete does exercise. The boxer-a type of athlete, also does boxing. Such overlapping common behaviors can be extracted into mixins. Let's create mixins next.

*Sketching: The `Sketching` mixin defines the common `sketch()` method. It takes a message and prints it on console to keep things simple for demonstration.*

```dart
mixin Sketching {
  sketch(String message) {
    print(message);
  }
}
```

*Reading: The `Reading` mixin defines `dailyReading(String topic)` method. A reading topic is passed as parameter.*

```dart
mixin Reading {
  dailyReading(String topic) {
    print("Daily reading on ${topic}");
  }
}
```

*Exercise: The `Exercise` mixin defines methods for running and weight training. The `running(int mile)` method passes `mile` parameter to print the message. The `weightTraining(int weights)` method prints the value for `weights` parameter.*

```dart
mixin Exercise {
  running(int mile) {
    print("Daily run of ${mile} mile(s)");
  }

  weightTraining(int weights) {
    print("Lifting ${weights} lbs");
  }
}
```

*Boxing: The `Boxing` mixin defines practicing punches behavior of an athlete. As per our requirements, only athletes are allowed to practice punches. In such cases, we may want to restrict the usage of mixin only by classes of type `Athlete`. We can apply such restriction on `Boxing` mixin by using `on` keyword followed by the ``Athlete` - the class which is allowed the usage.*

```dart
mixin Boxing on Athlete {
  punch(int n) {
    print("Boxer practicing ${n} punches");
  }
}
```

**Using Mixins**

`Artist` Class

```dart
class Artist extends Person with Sketching {
  sketchLandscape() {
    sketch("Making landscapes sketches");
  }
}
```

`Engineer` Class

```dart
class Engineer extends Person with Sketching, Reading {
  sketchBuildings() {
    sketch("Sketching engineering drawings");
  }

  readResearchPaper() {
    String topic = "Building Construction";
    dailyReading(topic);
  }
}
```

`Doctor` Class

```dart
class Doctor extends Person with Reading, Exercise {
  readReports() {
    String topic = "covid";
    dailyReading(topic);
  }

  workout() {
    running(1);
    weightTraining(10);
  }
}
```

`Athlete` Class

```dart
class Athlete extends Person with Exercise {
  generalRoutine() {
    running(2);
    weightTraining(20);
  }
}
```
`Boxer` Class

```dart
class Boxer extends Athlete with Boxing {
  punchPractice() {
    punch(100);
  }

  routineExercise() {
    running(4);
    weightTraining(40);
  }
}
```
Running Code

```dart
void main() {
  print("Artist");
  Artist artist = Artist();
  artist.sketchLandscape();

  print("\nEngineer");
  Engineer engineer = Engineer();
  engineer.sketchBuildings();
  engineer.readResearchPaper();

  print("\nDoctor");
  Doctor doctor = Doctor();
  doctor.readReports();
  doctor.workout();

  print("\nBoxer");
  Boxer boxer = Boxer();
  boxer.punchPractice();
  boxer.routineExercise();
}
```


**Q.4: What is callable class in Dart?**

**Ans.:** In Dart, functions are objects too. It's an object of type `Function`. Similar to other objects, functions can be passed as arguments to other functions, and can be assigned to variables as well.

A Callable class allows its instance to be called as function. This feature of Dart language is useful in making named-functions.

*Implementing Callable Class*

All Dart functions have `call` method. In order to make a class Callable, the `call()` method needs to be implemented. Let's declare a callable class below:

```dart
class Addition {
  int call(int a, int b) => a + b;
}
```
The above class' call method takes two arguments and returns their sum.

*Using Callable Class*

Let's check out using the `Addition` callable class in code below. The `addition` object is of `Addition` type. Now, `addition(1, 2)` can be called to calculate the sum of given numbers.

```dart
void main() {
  Addition addition = Addition();
  var result = addition(1, 2);
  print(result);
}
```

**Q.5: What are the generator functions in Dart?**
**Ans.:** Dart generator functions are used to generate sequence of values on-demand lazily. Such value sequence can be generated either synchronously or asynchronously. There are two types of built-in generator functions available to support both scenarios:

- Synchronous Generator: Synchronous generator function returns an `Iterable` object. That means first the values are generated and then returned lazily on-demand by the function.
    >Iterable: A collection of values, or "elements", that can be accessed sequentially.
- Asynchronous Generator: Asynchronous generator function returns a `Stream` object. The sequence of values is generated on demand as they become available.
    >Stream: A source of asynchronous data events.

**Q.4: How to create a singleton class in Dart?**

**Ans.:** Answer goes here.

**Q.5: What is `async`, `await` and `Future` in the Dart?**

**Ans.: ** Answer goes here.

**Q.6: What is difference between `async` and `async*` in Dart?**

**Ans.: ** Answer goes here.
