Core Java Interview Questions

**Q.1. What is difference between == and equals()?**
**Ans.:**
We use == for reference (address) comparison and equals() (of String class) for content comparison.

**Q.2. What is thread in Java**
**Ans.:**
A thread is an independent path of execution in the program. Multiple threads can run concurrently with in a program.

**Q.3. Constructor Chaining**
**Ans.:**
Constructor Chaining is the process of calling one constructor from another constructor with respect to current object.

Constructor Chianing can be done in two ways.. 
1- Within class using `this()`
2- from base class  using `super()` (inheritance is needed)

Super and this statements should always be the first line of the cunstrctor.

**Q.4. What is Annotation?**
**Ans.:** 
Annotations are tags that we can enter in our program code, for some tool to process it, and make sence of it.
1) Annotation starts with `@`
2) Annotation do not change the action of compiled program.
3) Annotations can change the way a program is teated by compiler 

There are three categories of Annotations
1) Marker Annotations: like `@Override`
2) Single value annotations: 
3) Full Annotations

**Q.4. What is difference between `wait()` and `sleep()`?**
**Ans.:** 

1- `wait()` can only be called from synchronized context whereas `sleep()` method can be called without synchronized block.
2- Using `wait()` method thread goes in waiting state and it would not come automatically untill we call `notify()` or `notifyAll()` method, whereas `sleep()` method
 pauses thread execution for specified time.
3- `wait()` releases the lock for other objects whereas `sleep()` method keeps lock for specified time or call interrupt.

**Q.5. What is `yield()` method?**
**Ans.:** 

`yield()` method is use to prevent the execution of a thread in between if something important is pending. `yield` basically means that the thread is not doing anything particularly important
and if any other thread need to be run, they should run. Otherwise the current thread will be continued.
Its gives hint to the thread schedular that its ready to pause its execution. Thread schedular is free to ignore this hint.

IF ANY THREAD EXECUTES YIELD METHOD, THREAD SCHEDULAR CHECKS IF THERE IS ANY THREAD WITH HIGHER OR SAME PRIORITY 
THAN THIS THREAD. IF SCHEDULAR FINDS ANY THREAD CURRENT THREAD WILL BE MOVED TO READY/RUNNABLE STATE AND GIVE PROCESSOR
 TO OTHER THREAD, IF NOT CURRENT THREAD WILL KEEP EXECUTING.

`Thread.yield();`

**Q.6. What is the difference between Process and Thread?**
**Ans.:** 
Process is basically a program in execution whereas Thread is a lightweight process or subprocess. 

**Q.7. What is `join()` method?**
**Ans.:**
 
`join()` method puts the current thread on wait until the thread on which it is called is dead. 
Its can be used to create a execution sequence for multiple thread to execute.

**Q.8. What is difference between `wait()` and `join()` method?**
**Ans.:**
 
Answer goes here.

**Q.9. What is thread scheduler?**
**Ans.:**
 
A thread scheduler is the part of the operating system in charge of deciding which threads in the system should run, when, and for how long. 
Android’s thread scheduler uses two main factors to determine how threads are scheduled across the entire system: nice values and cgroups.

**Q.10. What is the latest version of Java SE?**
**Ans.:**
 
Java SE 17.0. 2 (January 18, 2022). 

**Q.11. What is Looper?**
**Ans.:**
 
Looper is a class which is used to execute messages(runnable) in a queue. It is responsible to create a queue in a thread.

**Q.12. How `HashMap` stores the data?**
**Ans.:**
 
HashMap store data as key, value pairs. All the keys must be unique.
Whenever we create an object of `HashMap`, an array of bucket is created in the memory. Each bucket can be called a node.
When we put a key and value into hashmap, JVM calculates hashcode of the key and get an index (by some modular operations with hashCode).
 That index will be used to sotre and retrieve from array of buckets.
Each node points to a linked list which holds the data (Key, HashCode, Value, Address of next node). 

**Q.13. What is WeakHashMap?**
**Ans.:**
WeakHashMap is an implementation of Map interface that stores weak references only as key. Storing weak reference allows a key value pair to be garbage collected when it's key is no longer referenced outside to WeakHashMap.

```java
// Java program to illustrate
// WeakHashmap
import java.util.*;
class WeakHashMapDemo
{
    public static void main(String args[])throws Exception
    {
        WeakHashMap m = new WeakHashMap();
        Demo d = new Demo();
         
        // puts an entry into WeakHashMap
        m.put(d," Hi ");
        System.out.println(m);
         
        d = null;
         
        // garbage collector is called
        System.gc();
         
        // thread sleeps for 4 sec
        Thread.sleep(4000); .
         
        System.out.println(m);
    }
}
 
class Demo
{
    public String toString()
    {
        return "demo";
    }
     
    // finalize method
    public void finalize()
    {
        System.out.println("finalize method is called");
    }
} 
```

OUTPUT:

```java
{demo = Hi}
finalize method is called
{ }
```

**Q.14. What is Collection?**
**Ans.:** 

Collection is a framework provided in Java which provides multiple interfaces and classes to represent group of individual object as a single entity.

**Q.15. What is Iterator?**
**Ans.:** 

Iterator interface provides method to iterate over any collection. It does not allow any thread to modify object which is being iterated. It also allows to remove element while iterating.

**Q.16. What is `HashCode`?**
**Ans.:**

`hashCode()` method is a method of Object class that returns unique integer value for unique object.
This method is also used to search a value in collection.
JVM uses `hashCode` method while saving object into hashing related data structure.

**Q.17. What is dependency injection?**
**Ans.:**

Dependency injection is a concept, and according to this a class should not configure its dependencies statically but it should configured from outside.
A class has a dependency if it uses an instance of another class.

**Q.18. What is dependency injection?**
**Ans.:**

Dependency injection is a concept, and according to this a class should not configure its dependencies statically but it should configured from outside.
A class has a dependency if it uses an instance of another class.
