# FLUTTER INTERVIEW SECTION #

Below are the important questions of the Flutter.


**Q.1: What is Flutter?**

**Ans.: ** Flutter is an open-source UI software development kit created by Google. It is used to develop applications for Android, iOS, Linux, Mac, Windows, Google Fuchsia, and the web from a single codebase.

**Q.1: How the Flutter works?**

**Ans.: ** Answer goes here.

**Q.2: What are the lifecycle of a Flutter page?**

**Ans.: ** Statefull Widget Lifecycle 


1. `createState()`: When the framework is instructed to build a `StatefullWidget`, it immediately calls `createState()`.

2. `mounted` is true: When `createState` creates your state class, a `buildContext` is assigned to that state. `buildContext` is, overly simplified, the place in the widget tree in which this widget is placed. Here's a longer explanation. All widgets have a bool this.mounted property. It is turned true when the `buildContext` is assigned. It is an error to call `setState` when a widget is unmounted.

3. `initState()`: This is the first method called when the widget is created (after the class constructor, of course.) `initState` is called once and only once. It must call `super.initState()`

4. `didChangeDependencies()`: This method is called immediately after initState on the first time the widget is built.

5. `build()`: This method is called often. It is required, and it must return a Widget.

6. `didUpdateWidget(Widget oldWidget)`: If the parent widget changes and has to rebuild this widget (because it needs to give it different data), but it's being rebuilt with the same `runtimeType`, then this method is called. This is because Flutter is re-using the state, which is long lived. In this case, you may want to initialize some data again, as you would in `initState`.

7. `setState()`: This method is called often from the framework itself and from the developer. Its used to notify the framework that data has changed.

8. 	`deactivate()`: Deactivate is called when State is removed from the tree, but it might be reinserted before the current frame change is finished. This method exists basically because State objects can be moved from one point in a tree to another.

9. `dispose()`: Dispose is called when the `State` object is removed, which is permanent. This method is where you should unsubscribe and cancel all animations, streams, etc.

10. `mounted` is false: The state object can never remount, and an error is thrown when setState is called.


**Q.3: What are stateful and stateless widgets?**

**Ans.: ** A widget is either stateful or stateless. If a widget can change—when a user interacts with it, for example—it’s stateful.

A stateless widget never changes. `Icon`, `IconButton`, and `Text` are examples of stateless widgets. Stateless widgets subclass `StatelessWidget`.

A stateful widget is dynamic: for example, it can change its appearance in response to events triggered by user interactions or when it receives data. `Checkbox`, `Radio`, `Slider`, `InkWell`, `Form`, and `TextField` are examples of stateful widgets. Stateful widgets subclass `StatefulWidget`

A widget’s state is stored in a State object, separating the widget’s state from its appearance. The state consists of values that can change, like a slider’s current value or whether a `checkbox` is checked. When the widget’s state changes, the state object calls `setState()`, telling the framework to redraw the widget.

**Stateless :** Widget state creates ONLY ONCE, then it can update values but not state explicitly. That's why it has only one class which extends with StatelessWidget. They can never re-run build() method again.

**Stateful :** Widgets can update their STATE (locally) & values multiple times upon event triggered. That's the reason, the implementation is also different. In this, we have 2 classes, one is `StatefulWidget` & the other is it's State implementation handler i.e. State<YourWidget>. So if I say, they can re-run build() method again & again based on events triggered.

A `StatelessWidget` will never rebuild by itself (but can from external events). A `StatefulWidget` can.

A `StatelessWidget` is static wheres a `StatefulWidget` is dynamic.


**Q.4: What is the key in Flutter? How many types of keys are available?**

**Ans.: ** If we want to preserve the state of a widget when moved around the widget-tree, then we need use the key. State of a widget could be a property like color, checked/unchecked check box etc.

*Moving around the widget tree means, when we are moving a widget, dragging a widget, reordering a widget etc.*

If we are using a stateless widget then we don't need the key. The reason is, Flutter framework which builds the element tree (a skeleton of the widget tree, just has the information about type of the widget and reference to their child) does not need the key.

*Keys must be unique amongst the Elements with the same parent.*

Two implemetors of the `Key` are:

-**`GlobalKey`**: A key that is unique across the entire app. Global keys provide access to other objects that are associated with those elements, such as `BuildContext`.

For `StatefulWidgets`, global keys also provide access to State.

-**`LocalKey`**: A key that is not a `GlobalKey`.

Local keys must be unique amongst the Elements with the same parent.

**Q.5: How many ways are there to navigate to another page?**

**Ans.: ** Answer goes here.

**Q.6: What is inherited widget in Flutter?**

**Ans.: ** This is a special kind of widget that defines a context at the root of sub-tree. It can efficiently deliver the context to every widget in that sub-tree.

`MediaQuery`, `Style` are the example of `InheritedWidget`.

Watch what happens when we want to pass `accountId` and `scopeId` from the page to a widget two levels below: 


```
class MyPage extends StatelessWidget {
  final int accountId;
  final int scopeId;
  
  MyPage(this.accountId, this.scopeId);
  
  Widget build(BuildContext context) {
    return new MyWidget(accountId, scopeId);
  }
}

class MyWidget extends StatelessWidget {
  final int accountId;
  final int scopeId;
  
  MyWidget(this.accountId, this.scopeId);
  
  Widget build(BuildContext context) {
    // somewhere down the line
    new MyOtherWidget(accountId, scopeId);
    ...
  }
}

class MyOtherWidget extends StatelessWidget {
  final int accountId;
  final int scopeId;
  
  MyOtherWidget(this.accountId, this.scopeId);
  
  Widget build(BuildContext context) {
    // rinse and repeat
    ...

```

MyWidget’s state is independent of the parameters and yet it was getting rebuilt every time the parameters were changing!

If we augment the example above using inherited widgets, here’s what we get:


```
class MyInheritedWidget extends InheritedWidget {
  final int accountId;
  final int scopeId;

  MyInheritedWidget(accountId, scopeId, child): super(child);
  
  @override
  bool updateShouldNotify(MyInheritedWidget old) =>
    accountId != old.accountId || scopeId != old.scopeId;
}

class MyPage extends StatelessWidget {
  final int accountId;
  final int scopeId;
  
  MyPage(this.accountId, this.scopeId);
  
  Widget build(BuildContext context) {
    return new MyInheritedWidget(
      accountId,
      scopeId,
      const MyWidget(),
     );
  }
}

class MyWidget extends StatelessWidget {

  const MyWidget();
  
  Widget build(BuildContext context) {
    // somewhere down the line
    const MyOtherWidget();
    ...
  }
}


```

**Q.6: What is the `setState()` in Flutter?**

**Ans.: ** `setState()` notifies the framework that the internal state of this object has changed.

**Q.7: Flutter is multithreaded or single threaded?**

**Ans.: ** Answer goes here.

**Q.9: What is Dart and why does Flutter use it?**

**Ans.: ** Dart is an object-oriented, garbage-collected programming language that you use to develop Flutter apps. It was also created by Google, but is open-source, and has community inside and outside Google. Dart was chosen as the language of Flutter for the following reason:

- Dart is AOT (Ahead Of Time) compiled to fast, predictable, native code, which allows almost all of Flutter to be written in Dart. This not only makes Flutter fast, virtually everything (including all the widgets) can be customized.
- Dart can also be JIT (Just In Time) compiled for exceptionally fast development cycles and game-changing workflow (including Flutter’s popular sub-second stateful hot reload).
- Dart allows Flutter to avoid the need for a separate declarative layout language like JSX or XML, or separate visual interface builders, because Dart’s declarative, programmatic layout is easy to read and visualize. And with all the layout in one language and in one place, it is easy for Flutter to provide advanced tooling that makes layout a snap

**Q.10: What is a "widget" and mention its importance in Flutter?**

**Ans.: ** Widgets are basically the UI components in Flutter.
- It is a way to describe the configuration of an Element.
- They are inspired from components in React

**Q.11: What is an App state?**

**Ans.: ** State that is not ephemeral, that you want to share across many parts of your app, and that you want to keep between user sessions, is what we call application state (sometimes also called shared state).
Examples of application state: 
- User preferences 
- Login info 
- Notifications in a social networking app 
- The shopping cart in an e-commerce app 
- Read/unread state of articles in a news app

**Q.12: What is the difference between "main()" and "runApp()" functions in Flutter?**

**Ans.: **

- `main ()` function came from Java-like languages so it's where all program started, without it, you can't write any program on Flutter even without UI.

- `runApp()` function should return Widget that would be attached to the screen as a root of the Widget Tree that will be rendered.

**Q.13: Differentiate between Hot Restart and Hot Reload?**

**Ans.:**

**Hot Reload**

- Flutter hot reload features works with combination of Small r key on command prompt or Terminal.
- Hot reload feature quickly compile the newly added code in our file and sent the code to Dart Virtual Machine. After done updating the Code Dart Virtual Machine update the app UI with widgets.
- Hot Reload takes less time then Hot restart.
- There is also a draw back in Hot Reload, If you are using States in your application then Hot Reload preservers the States so they will not update on Hot Reload our set to their default values.

**Hot Restart**

- Hot restart is much different than hot reload.
- In Hot restart it destroys the preserves State value and set them to their default. So if you are using States value in your application then after every hot restart the developer gets fully compiled application and all the states will be set to their defaults.
- The app widget tree is completely rebuilt with new typed code.
- Hot Restart takes much higher time than Hot reload

**Q.14: Differentiate between required and optional parameters in Dart?**

**Ans.:** 

**Required Parameters**

Dart required parameters are the arguments that are passed to a function and the function or method required all those parameters to complete its code block.

```
findVolume(int length, int breath, int height) {
 print('length = $length, breath = $breath, height = $height');
}

findVolume(10,20,30);

```

**Optional Parameters**

Optional parameters are defined at the end of the parameter list, after any required parameters.
In Flutter/Dart, there are 3 types of optional parameters: - Named - Parameters wrapped by { } - eg. getUrl(int color, [int favNum]) - Positional - Parameters wrapped by [ ]) - eg. getUrl(int color, {int favNum}) - Default - Assigning a default value to a parameter. - eg. getUrl(int color, [int favNum = 6])

**Q.15: Explain the different types of Streams?**

**Ans.: ** There are two kinds of streams. 1. Single subscription streams - The most common kind of stream. - It contains a sequence of events that are parts of a larger whole. Events need to be delivered in the correct order and without missing any of them. - This is the kind of stream you get when you read a file or receive a web request. - Such a stream can only be listened to once. Listening again later could mean missing out on initial events, and then the rest of the stream makes no sense. - When you start listening, the data will be fetched and provided in chunks.

**Broadcast streams**

- It intended for individual messages that can be handled one at a time. This kind of stream can be used for mouse events in a browser, for example.
- You can start listening to such a stream at any time, and you get the events that are fired while you listen.
- More than one listener can listen at the same time, and you can listen again later after canceling a previous subscription.

**Q.16: What are Null-aware operators?**

**Ans.: **

- Dart offers some handy operators for dealing with values that might be null.
- One is the ??= assignment operator, which assigns a value to a variable only if that variable is currently null:


```

    int a; // The initial value of a is null.
	a ??= 3;
	print(a); // <-- Prints 3.

	a ??= 5;
	print(a); // <-- Still prints 3.
```

- Another null-aware operator is ??, which returns the expression on its left unless that expression’s value is null, in which case it evaluates and returns the expression on its right:

```
    print(1 ?? 3); // <-- Prints 1.
	print(null ?? 12); // <-- Prints 12.


```

**Q.17: What is ScopedModel / BLoC Pattern?**

**Ans.:** 

**Q.17: What are keys in Flutter and when to use it?**

**Ans.: **

- A Key is an identifier for Widgets, Elements and SemanticsNodes.
- A new widget will only be used to update an existing element if its key is the same as the key of the current widget associated with the element.
- Keys must be unique amongst the Elements with the same parent.
- Subclasses of Key should either subclass LocalKey or GlobalKey.
- Keys are useful when manipulating collections of widgets of the same type.
- If you find yourself adding, removing, or reordering a collection of widgets of the same type that hold some state, then, you should use a key.

**Q.18: What is ScopedModel / BLoC Pattern?**

**Ans.:** ScopedModel and BLoC (Business Logic Components) are common Flutter app architecture patterns to help separate business logic from UI code and using fewer Stateful Widgets.

- **Scoped Model** is a third-party package that is not included into Flutter framework. It's a set of utilities that allow you to easily pass a data Model from a parent Widget down to its descendants. In addition, it also rebuilds all of the children that use the model when the model is updated. This library was originally extracted from the Fuchsia codebase.

- **BLoC** stands for Business Logic Components. It helps in managing state and make access to data from a central place in your project. The gist of BLoC is that everything in the app should be represented as stream of events: widgets submit events; other widgets will respond. BLoC sits in the middle, managing the conversation.

**Q.18: What is Streams in Flutter/Dart?**

**Ans.: ** A source of asynchronous data events.

A Stream provides a way to receive a sequence of events. Each event is either a data event, or an error event, which is a notification that something has failed. When a stream has emitted all its event, a single "done" event will notify the listener that the end has been reached.

You listen on a stream to make it start generating events, and to set up listeners that receive the events. When you listen, you receive a StreamSubscription object which is the active object providing the events, and which can be used to stop listening again, or to temporarily pause events from the subscription.

There are two kinds of streams: **Single-subscription** streams and **broadcast** streams.

A **single-subscription stream** allows only a single listener during the whole lifetime of the stream. It doesn't start generating events until it has a listener, and it stops sending events when the listener is unsubscribed, even if the source of events could still provide more.

Listening twice on a single-subscription stream is not allowed, even after the first subscription has been canceled.

Single-subscription streams are generally used for streaming chunks of larger contiguous data like file I/O.

A **broadcast stream** allows any number of listeners, and it fires its events when they are ready, whether there are listeners or not.

Broadcast streams are used for independent events/observers.

If several listeners want to listen to a single subscription stream, use asBroadcastStream to create a broadcast stream on top of the non-broadcast stream.

```

Future<int> sumStream(Stream<int> stream) async {
	  var sum = 0;
	  await for (var value in stream) {
	    sum += value;
	  }
	  return sum;
	}

```

- Streams provide an asynchronous sequence of data.
- Data sequences include user-generated events and data read from files.
- You can process a stream using either await for or listen() from the Stream API.
- Streams provide a way to respond to errors.
- There are two kinds of streams: single subscription or broadcast.

**Q.19: Explain async, await in Flutter/Dart?**

**Ans.:** Answer goes here

**Q.20: How does Dart AOT work?**

**Ans.:** Answer goes here

**Q.21: What is Future in Flutter/Dart?**

**Ans.:** A Future is used to represent a potential value, or error, that will be available at some time in the future.

```

// asynchronous data 
main() async {
  String x = await HelloAsync();
  print(x);
}

Future<String> HelloAsync() async{
   await Future.delayed(Duration(seconds:5));
   return 'Message from Future.';
}

```

**Q.22: What is a difference between these operators "?? and ?**

**Ans.:** Answer goes here

**Q.23: What is the difference between double.INFINITY and MediaQuery?**

**Ans.:** Answer goes here

**Q.24: How stream and Future are similar?**

**Ans.:** 

- Both works asynchronously
- Both have some potential value

**Q.25: How stream and Future are different?**

**Ans.:**

- A stream is a combination of Futures
- Future has only one response but Stream could have any number of Response.


