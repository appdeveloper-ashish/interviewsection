# INTERVIEW SECTION #

This file will have content that are very important from interview point of view. It will have content of Java, Android, SQL, SQLite, Kotlin etc.

## Kotlin ##

**Q.1. How does Kotlin work on Android?**

**Ans.:** Just like Java, the Kotlin code is also compiled into the Java bytecode and is executed at runtime by the Java Virtual Machine i.e. JVM. When a Kotlin file named Main.kt is compiled then it will eventually turn into a class and then the bytecode of the class will be generated. The name of the bytecode file will be MainKt.class and this file will be executed by the JVM.

**Q.2. Why should we use Kotlin?**

**Ans.:**

- Kotlin is concise
- Kotlin is null-safe
- Kotlin is interoperable

Watch this video for more details. https://www.youtube.com/watch?v=kRhivT-jKzY&list=PL6nth5sRD25iv8jZrQWD-5dXgu56ae5m8

**Q.3. What is the difference between the variable declaration with var and val?**

**Ans.:** If you want to declare some mutable(changeable) variable, then you can use `var`. For the immutable variable, use `val` i.e. `val` variables can't be changed once assigned.

**Q.4. What is the difference between the variable declaration with val and const?** 

**Ans.:** Both the variables that are declared with val and const are immutable in nature. But the value of the const variable must be known at the compile-time whereas the value of the val variable can be assigned at runtime also.

**Q.5. How to ensure null safety in Kotlin?**

**Ans.:** One of the major advantages of using Kotlin is null safety. In Java, if you access some null variable then you will get a `NullPointerException`. So, the following code in Kotlin will produce a compile-time error:

```
var name: String = "MindOrks"
name = null //error
```

So, to assign null values to a variable, you need to declare the name variable as a nullable string and then during the access of this variable, you need to use a safe call operator i.e. ?.

```
var name: String? = "MindOrks"
print(name?.length) // ok
name = null // ok
```

You can learn more on: https://blog.mindorks.com/safecalls-vs-nullchecks-in-kotlin

**Q.6. What is the difference between safe calls(?.) and null check(!!)?**

**Ans.:** Safe call operator i.e. `?.` is used to check if the value of the variable is null or not. If it is null then null will be returned otherwise it will return the desired value.

```
var name: String? = "MindOrks"
println(name?.length) // 8
name = null
println(name?.length) // null

```
If you want to throw `NullPointerException` when the value of the variable is null, then you can use the null check or `!!` operator.

```
var name: String? = "MindOrks"
println(name?.length) // 8
name = null
println(name!!.length) 
```

Learn more on https://blog.mindorks.com/safecalls-vs-nullchecks-in-kotlin

**Q.7. Do we have a ternary operator in Kotlin just like java?**

**Ans.:** No, we don't have a ternary operator in Kotlin but you can use the functionality of ternary operator by using if-else or Elvis operator.

**Q.8. What is Elvis operator in Kotlin?**

**Ans.:** In Kotlin, you can assign null values to a variable by using the null safety property. To check if a value is having null value then you can use if-else or can use the Elvis operator i.e. `?:` For example:
```
var name:String? = "Mindorks"
val nameLength = name?.length ?: -1
println(nameLength)
```
The Elvis operator(`?:`) used above will return the length of name if the value is not null otherwise if the value is null, then it will return `-1`.

**Q.9. How to convert a Kotlin source file to a Java source file?**

**Ans.:** Steps to convert your Kotlin source file to Java source file:

- Open your Kotlin project in the IntelliJ IDEA / Android Studio.
- Then navigate to Tools > Kotlin > Show Kotlin Bytecode.
- Now click on the Decompile button to get your Java code from the bytecode.

**Q.10. What is the use of @JvmStatic, @JvmOverloads, and @JvmFiled in Kotlin?**

**Ans.:** 
- `@JvmStatic:` This annotation is used to tell the compiler that the method is a static method and can be used in Java code.
- `@JvmOverloads:` To use the default values passed as an argument in Kotlin code from the Java code, we need to use the @JvmOverloads annotation.
- `@JvmField:` To access the fields of a Kotlin class from Java code without using any getters and setters, we need to use the @JvmField in the Kotlin code.

**Q.11. What is a data class in Kotlin?**

**Ans.:** Data classes are those classes which are made just to store some data. In Kotlin, it is marked as data. The following is an example of the same:
```kotlin
data class Developer(val name: String, val age: Int)
```
When we mark a class as a data class, you don’t have to implement or create the following functions like we do in Java: hashCode(), equals(), toString(), copy(). The compiler automatically creates these internally, so it also leads to clean code. Although, there are few other requirements that data classes need to fulfill.

**Q.12. Can we use primitive types such as int, double, float in Kotlin?**

**Ans.:** In Kotlin, we can't use primitive types directly. We can use classes like Int, Double, etc. as an object wrapper for primitives. But the compiled bytecode has these primitive types.

**Q.13. What is String Interpolation in Kotlin?**

**Ans.:** If you want to use some variable or perform some operation inside a string then String Interpolation can be used. You can use the `$` sign to use some variable in the string or can perform some operation in between `{}` sign.
```kotlin
var name = "MindOrks"
print("Hello! I am learning from $name")
```
**Q.14. What do you mean by destructuring in Kotlin?**

**Ans.:** **Destructuring** is a convenient way of extracting multiple values from data stored in(possibly nested) objects and Arrays. It can be used in locations that receive data (such as the left-hand side of an assignment). Sometimes it is convenient to destructure an object into a number of variables, for example:
```kotlin
val (name, age) = developer
```
Now, we can use name and age independently like below:
```kotlin
println(name)
println(age)
```

**Q.15. When to use the lateinit keyword in Kotlin?**

**Ans.:** The `lateinit` keyword is used for late initialization of variables.

There are many cases when you need to create a variable but you don't want to initialize it at the time of declaration/creation of the variable and you don't want to create it as a nullable variable then you can use `lateinit`.

```kotlin
private lateinit var courseName: String
// demo function to get course name using the courseId
fun fetchCourseName(courseId: String) {
    courseName = courseRepository.getCourseName(courseId)
    // this is an example, you can add other suff according to your usecase
}
```

You should be very sure that your lateinit variable will be initialized before accessing it otherwise you will get:
```kotlin
UninitializedPropertyAccessException: lateinit property courseName has not been initialized
```
**Usage of lateinit**
- For late initialization of variables.
- For injecting an object using Dagger.

**Q.16. How to check if a lateinit variable has been initialized or not?**

**Ans.:** You can check if the lateinit variable has been initialized or not before using it with the help of `isInitialized` method. This method will return true if the lateinit property has been initialized otherwise it will return false. For example:
```kotlin
class Person {
 lateinit var name: String
 fun initializeName() {
 println(this::name.isInitialized)
 name = "MindOrks" // initializing name
 println(this::name.isInitialized)
 }
}
fun main(args: Array<String>) {
 Person().initializeName()
}
```
**Q.17. What is the difference between lateinit and lazy in Kotlin?**
**Ans.:** 

**Lateinit Keyword** *Lateinit keyword is used for late initialization of variables.*

There are many cases when we need to to create a variable but we do not want to initialize it at the time of declaration or creation of variable.
We can make it either nullable like below:
```kotlin
private var courseId: String? = null
```
or, we can use `lateinit` keyword.
```kotlin
private lateinit var courseName: String
// demo function to get course name using the courseId
fun fetchCourseName(courseId: String) {
    courseName = courseRepository.getCourseName(courseId)
    // this is an example, you can add other suff according to your usecase
}
```
You should be very sure that your lateinit variable will be initialized before accessing it otherwise you will get:
```kotlin
UninitializedPropertyAccessException: lateinit property courseName has not been initialized
```
**Usage of lateinit**
- For late initialization of variables
- For injecting an object using Dagger

**Lazy Kayword**
There are certain classes whose object initialization is very heavy and so much time taking that it results in the delay of the whole class creation process.
For example, let's say you have a class named `HeavyClass` and you need an object of this `HeavyClass` in some other class named `SomeClass`:
```kotlin
class SomeClass {
    private val heavyObject: HeavyClass = HeavyClass()
}
```
Here, we are creating a heavy object and this will result in slow or delayed creation of the SomeClass. There may be cases where you may not need the HeavyClass object. So, in this case, the lazy keyword can help you:
```kotlin
class SomeClass {
     private val heavyObject: HeavyClass by lazy {
        HeavyClass()
    }  
}
```
**Benifit of lazy initialization**
- The benefit of using lazy is that the object will be created only when it is called otherwise it will not be created.
- The other benefit of using lazy is that once the object is initialized, you will use the same object again when called.
Example:
```kotlin
class SomeClass {
    private val heavyObject: HeavyClass by lazy {
        println("Heavy Object initialised")
        HeavyClass()
    } 
    
    fun accessObject() {
        println(heavyObject)
    }
}

fun main(args: Array<String>) {
    val someClass = SomeClass()
    println("SomeClass initialised")
    someClass.accessObject()
    someClass.accessObject()
}
```
The output of the above code is:
```kotlin
SomeClass initialised 
Heavy Object initialised 
HeavyClass@2a84aee7 
HeavyClass@2a84aee7
```
**Q.18. Is there any difference between == operator and === operator?**
**Ans.:** Yes. The `==` operator is used to compare the values stored in variables and the `===` operator is used to check if the reference of the variables are equal or not. But in the case of primitive types, the `===` operator also checks for the value and not reference.
```kotlin
// primitive example
val int1 = 10 
val int2 = 10
println(int1 == int2) // true
println(int1 === int2) // true
// wrapper example
val num1 = Integer(10)
val num2 = Integer(10)
println(num1 == num2) // true
println(num1 === num2) //false
```
**Q.19 What are companion objects in Kotlin?**
**Ans.:** In Kotlin, if you want to write a function or any member of the class that can be called without having the instance of the class then you can write the same as a member of a companion object inside the class.
To create a companion object, you need to add the companion keyword in front of the object declaration.
The following is an example of a companion object in Kotlin:
```kotlin
class ToBeCalled {
 companion object Test {
 fun callMe() = println("You are calling me :)")
 }
}
fun main(args: Array<String>) {
 ToBeCalled.callMe()
}
```
**Q.20 What is the equivalent of Java static methods in Kotlin?**
**Ans.:** To achieve the functionality similar to Java static methods in Kotlin, we can use:

- companion object
- package-level function
- object
**Q.21 What is the difference between FlatMap and Map in Kotlin?**
**Ans.:** 
- `FlatMap` is used to combine all the items of lists into one list.
- `Map` is used to transform a list based on certain conditions.
**Q.22 What are visibility modifiers in Kotlin?**
**Ans.:** A visibility modifier or access specifier or access modifier is a concept that is used to define the scope of something in a programming language. In Kotlin, we have four visibility modifiers:
- **private**: visible inside that particular class or file containing the declaration.
- **protected**:visible inside that particular class or file and also in the subclass of that particular class where it is declared.
- **internal**:visible everywhere in that particular module.
- **public**:visible to everyone.

*Note: By default, the visibility modifier in Kotlin is public.*


**Q.23 How to create a Singleton class in Kotlin?**
**Ans.:** A singleton class is a class that is defined in such a way that only one instance of the class can be created and is used where we need only one instance of the class like in logging, database connections, etc.

To create a Singleton class in Kotlin, you need to use the object keyword.
```kotlin
object AnySingletonClassName
```
*Note: You can't use constructor in object, but you can use init*

In Kotlin, we need to use the **object** keyword to use Singleton class. The **object** class can have functions, properties, and the **init** method. The constructor method is not allowed in an object so we can use the init method if some initialization is required and the object can be defined inside a class. The object gets instantiated when it is used for the first time.

Let’s have an example of the Singleton class in Kotlin.

```kotlin
object Singleton{
    
    init {
        println("Singleton class invoked.")
    }
    var variableName = "I am Var"
    fun printVarName(){
        println(variableName)
    }

}

fun main(args: Array<String>) {     
    Singleton.printVarName()
    Singleton.variableName = "New Name"
        
    var a = A()
}

class A {

    init {
        println("Class init method. Singleton variableName property : ${Singleton.variableName}")
        Singleton.printVarName()
    }
}
```

**Q.24 What are init blocks in Kotlin?**
**Ans.:** `init` blocks are initializer blocks that are executed just after the execution of the primary constructor. A class file can have one or more `init` blocks that will be executed in series. If you want to perform some operation in the primary constructor, then it is not possible in Kotlin, for that, you need to use the `init` block.

**Q.25 What are the types of constructors in Kotlin?**
**Ans.:** 
- **Primary constructor:** These constructors are defined in the class header and you can't perform some operation in it, unlike Java's constructor.
```kotlin
class Person constructor(name: String, age: Int, salary: Int) {
}
```
If you are not having any annotations or modifiers(public, private, protected), then you can omit the constructor keyword like this:
```kotlin
class Person (name: String, age: Int, salary: Int) {
}
```
- **Secondary constructor:** These constructors are declared inside the class body by using the constructor keyword. You must call the primary constructor from the secondary constructor explicitly. Also, the property of the class can’t be declared inside the secondary constructor. There can be more than one secondary constructors in Kotlin.
```kotlin

class Student (var name: String) {
    init() {
        println("Student has got a name as $name")
    }

    constructor(sectionName: String, id: Int) this(sectionName) {

    }
}
```
